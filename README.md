# TESTE SOROCABACOM
## Resultado: [https://testesorocabacom.vercel.app/](https://testesorocabacom.vercel.app/)

## :watch: Cronômetro

![Cronometro](./images/toggl.png)

> teve algumas pausas, mas esqueci de pausar o cronômetro.

## :rocket: Tecnologias Utilizadas

* ReactJS
* React-elastic-carousel

## :construction_worker: Executar o projeto

```bash
# Clone Repository
$ git clone https://samaraferreira@bitbucket.org/samaraferreira/teste-sorocabacom.git

# Go to web folder
$ cd teste-sorocabacom

# Install Dependencies
$ yarn install || npm install

# Run Aplication
$ yarn start || npm start
```
Go to [http://localhost:3000/](http://localhost:3000/) to see the result.

