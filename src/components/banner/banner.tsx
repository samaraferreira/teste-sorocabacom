import React from 'react'

import './banner-styles.css'
import bannerImage from '../../assets/banner-image.png'

const Banner: React.FC = () => {
  return (
    <section className="banner">
      <div className="banner__card">
        <strong className="banner__card-title">
          TRANSISTOR - RED THE SINGER
        </strong>
        <img className="banner__card-image" src={bannerImage} alt="" />
        <p className="banner__card-text">
          "Olha, o que quer que você esteja pensando, me faça um favor, não
          solte."
        </p>
      </div>
    </section>
  )
}

export default Banner
