import React from 'react'

import './modal-styles.css'

import successIcon from '../../../assets/success.svg'

type ModalProps = {
  active: boolean,
  setActive: React.Dispatch<React.SetStateAction<boolean>>
}

const Modal: React.FC<ModalProps> = ({ active, setActive }: ModalProps) => {
  return (
    <>
      {active && (
        <div className="wrapper">
          <div className="message-success">
            <img className="message-success__icon" src={successIcon} alt="Mensagem enviada"/>
            <strong className="message-success__text">Mensagem enviada com sucesso!</strong>
            <button className="message-success__btn" onClick={() => setActive(false)}>OK</button>
          </div>
        </div>
      )}
    </>
  )
}

export default Modal
