import React from 'react'
import { Banner, Carousel, Form, Header } from './components'

import './styles/global.css'
import arrowTopIcon from './assets/arrow-top.svg'

const App: React.FC = () => {
  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }

  return (
    <>
      <Header />
      <Banner />
      <Carousel />
      <Form />
      <footer className="footer">
        <button className="go-top-btn" onClick={scrollTop}>
          <img src={arrowTopIcon} alt="Voltar para o topo da página"/>
        </button>
      </footer>
    </>
  )
}

export default App
