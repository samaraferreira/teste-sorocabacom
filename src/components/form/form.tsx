import React, { useState } from 'react'

import './form-styles.css'

import Modal from './modal/modal'

const Form: React.FC = () => {
  const [state, setState] = useState({
    name: '',
    email: '',
    message: ''
  })
  const [errors, setErrors] = useState({
    nameError: false,
    emailError: false,
    messageError: false
  })
  const [activeModal, setActiveModal] = useState(false)

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    const field = event.target.name
    const value = event.target.value
    setState((old) => ({ ...old, [field]: value }))
    validate(field, value)
  }

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    validate('name', state.name)
    validate('email', state.email)
    validate('message', state.message)

    if (state.name && state.email && state.message) {
      setActiveModal(true)
    }
  }

  const validate = (field: String, value: String) => {
    setErrors((old) => ({ ...old, [`${field}Error`]: !value && true }))
  }

  return (
    <>
      <Modal active={activeModal} setActive={setActiveModal} />
      <section className="form-section">
        <div className="form-container">
          <form onSubmit={handleSubmit} className="form">
            <h1 className="form__title">FORMULÁRIO</h1>
            <p className="form__description">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
            <div className="input-group">
              <div className="input-container">
                <input
                  className={`input ${
                    errors.nameError ? 'input__border--error' : ''
                  }`}
                  onChange={(e) => handleChange(e)}
                  type="text"
                  name="name"
                  placeholder="Nome"
                />
                {errors.nameError && (
                  <span className="error">Nome é obrigatório</span>
                )}
              </div>
              <div className="input-container">
                <input
                  className={`input ${
                    errors.emailError ? 'input__border--error' : ''
                  }`}
                  onChange={(e) => handleChange(e)}
                  type="email"
                  name="email"
                  placeholder="Email"
                />
                {errors.emailError && (
                  <span className="error">Email é obrigatório</span>
                )}
              </div>
            </div>
            <div className="textarea-container">
              <textarea
                className={`textarea ${
                  errors.messageError ? 'input__border--error' : ''
                }`}
                onChange={(e) => handleChange(e)}
                name="message"
                placeholder="Mensagem"
              />
              {errors.messageError && (
                <span className="error">Mensagem é obrigatória</span>
              )}
            </div>

            <button className="form__button" type="submit">
              ENVIAR
            </button>
          </form>
        </div>
      </section>
    </>
  )
}

export default Form
