import React from 'react'
import ElasticCarousel from 'react-elastic-carousel'

import './carousel-styles.css'

import GrantImg from '../../assets/Grant.png'
import RedImg from '../../assets/Red.png'
import SybilImg from '../../assets/Sybil.png'

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 650, itemsToShow: 2, itemsToScroll: 2 },
  { width: 900, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 }
]

const Carousel: React.FC = () => {
  return (
    <div className="carousel-container">
      <ElasticCarousel
        breakPoints={breakPoints}
        isRTL={false}
        pagination={false}
      >
        <li className="card">
          <div className="card__image">
            <img src={GrantImg} alt="Grant" />
          </div>
          <p className="card__text">
            A Camerata foi apenas os dois no início, e suas fileiras nunca foram
            destinadas a exceder um número a ser contado em uma mão.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={RedImg} alt="Red" />
          </div>
          <p className="card__text">
            Red, uma jovem cantora, entrou em posse do Transistor. Sendo a
            poderosa espada falante. O grupo Possessores quer tanto ela quanto o
            Transistor e está perseguindo implacavelmente a sua procura.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={SybilImg} alt="Grant" />
          </div>
          <p className="card__text">
            Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da
            Camerata.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={GrantImg} alt="Grant" />
          </div>
          <p className="card__text">
            A Camerata foi apenas os dois no início, e suas fileiras nunca foram
            destinadas a exceder um número a ser contado em uma mão.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={RedImg} alt="Red" />
          </div>
          <p className="card__text">
            Red, uma jovem cantora, entrou em posse do Transistor. Sendo a
            poderosa espada falante. O grupo Possessores quer tanto ela quanto o
            Transistor e está perseguindo implacavelmente a sua procura.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={SybilImg} alt="Grant" />
          </div>
          <p className="card__text">
            Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da
            Camerata.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={GrantImg} alt="Grant" />
          </div>
          <p className="card__text">
            A Camerata foi apenas os dois no início, e suas fileiras nunca foram
            destinadas a exceder um número a ser contado em uma mão.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={RedImg} alt="Red" />
          </div>
          <p className="card__text">
            Red, uma jovem cantora, entrou em posse do Transistor. Sendo a
            poderosa espada falante. O grupo Possessores quer tanto ela quanto o
            Transistor e está perseguindo implacavelmente a sua procura.
          </p>
        </li>
        <li className="card">
          <div className="card__image">
            <img src={SybilImg} alt="Grant" />
          </div>
          <p className="card__text">
            Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da
            Camerata.
          </p>
        </li>
      </ElasticCarousel>
    </div>
  )
}

export default Carousel
