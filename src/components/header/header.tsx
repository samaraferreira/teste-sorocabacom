import React from 'react'

import logo from '../../assets/logo.png'

import './header-styles.css'

const Header: React.FC = () => {
  return (
    <header className="header">
      <img className="header__logo" src={logo} alt="Super Giant Games" />
      <h1 className="header__title">SuperGiantGames</h1>
    </header>
  )
}

export default Header
